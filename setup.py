#!/usr/bin/env python

from distutils.core import setup

setup(name         = 'puff',
      version      = '0.1',
      description  = 'Share files with your friends using puff',
      author       = 'David Villa',
      author_email = '<david.villa@uclm.es>',
      url          = 'https://arco.inf-cr.uclm.es/svn/public/prj/puff',
      license      = 'GPL v3',
#      data_files   = [('share/man/man1',['puff.1'])],
      scripts      = ['puff.py']
      )
