#!/usr/bin/env python
# -*- coding: utf-8 -*-

# sample config file
'''
[server]
host: example.com
ssh_dir: public_html/paste
web_dir: ~lusuario/paste
'''


# TODO
#
# - Un botón "cancel" en el dialogo de progreso
# - Lista de ficheros subidos en un TreeView
# - Tiempo hasta borrado automático
# - Convertir en applet
# - Previsualización de imágenes en el FileChooser
# - DnD sobre la lista
# - DnD sobre el applet añade el fichero directamente

import os, sys
import ConfigParser
import signal, subprocess

import gobject, gtk


def gtk_do_pending():
    while gtk.events_pending():
        gtk.main_iteration(True)


class FileShare:
    def __init__(self, files):
        self.win = gtk.Window()
        self.win.connect("destroy", gtk.main_quit)

        #self.bt_add = gtk.Button(stock=gtk.STOCK_ADD)
        # self.bt_add.connect("clicked", self.on_button_add_clicked)
        #
        # self.win.add(self.bt_add)
        # self.win.show_all()

        #self.on_button_add_clicked(self.bt_add)

        self.preview = gtk.Image()

        if files:
            self.upload_file(files[0])
        else:
            self.select_file()

    def update_progress(self, n, text = None):
        if text:
            self.pb.set_text(text)
        else:
            self.pb.set_fraction(float(n) / 100)
            self.pb.set_text("puffing: %s %%" % n)

        gtk_do_pending()

    def upload_file(self, path):
        dst_name = os.path.basename(path)

        filesize = os.stat(path).st_size
        print filesize

        cmd = "cat \"{fpath}\" | pv -n -s {size} | ssh {host} 'cat - > \"{remote_dir}/{remote_fname}\"'".format(
            fpath        = path,
            size         = filesize,
            host         = config.get("server", "host"),
            remote_dir   = config.get("server", "ssh_dir"),
            remote_fname = dst_name)

        uri = 'http://{host}/{web_dir}/{fname}'.format(
            host    = config.get("server", "host"),
            web_dir = config.get("server", "web_dir"),
            fname   = dst_name)

        #dialog = gtk.Dialog(title="Uploading file",
        #                    flags=gtk.DIALOG_NO_SEPARATOR)

        w = gtk.Window(type=gtk.WINDOW_POPUP)
        w.move(0, 26)
        w.set_border_width(2)
        w.set_modal(True)

        box = gtk.HBox()
        w.add(box)

        print w
        self.pb = gtk.ProgressBar()
        self.pb.set_size_request(120, 23)
        self.pb.set_text('0 %')
        self.pb.set_fraction(0)
        box.pack_start(self.pb, True, True, 2)

        self.bt = gtk.Button("Cancel")
        self.bt.connect("clicked", gtk.main_quit)
        self.bt.set_size_request(70, -1)
        box.pack_start(self.bt)

        w.show_all()

        signal.signal(signal.SIGPIPE, signal.SIG_DFL)
        self.p = subprocess.Popen(cmd, shell=True,
                             stderr=subprocess.STDOUT, stdout=subprocess.PIPE)

        gobject.timeout_add(1000, self.ps_control, uri)

    def ps_control(self, uri):
        pid, rcode = os.waitpid(self.p.pid, os.WNOHANG)
        if pid == 0:
            line = self.p.stdout.readline().strip()
            if line:
                print line
                self.update_progress(int(line))
            return True

        assert rcode == 0
        print ("Ready, rcode: '%s'" % rcode)

        self.update_progress(100)
        clip = gtk.Clipboard(selection="PRIMARY")
        clip.set_text(uri)

        self.time = 20
        self.pb.set_size_request(50, -1)
        self.update_progress(100, "Ready")
        gobject.timeout_add(1000, self.update_button)
        return False

    def on_button_add_clicked(self, w):
        self.select_file()

    def select_file(self):
        chooser = gtk.FileChooserDialog(
            title = 'Select file',
            action = gtk.FILE_CHOOSER_ACTION_OPEN,
            buttons = (gtk.STOCK_CANCEL,
                       gtk.RESPONSE_CANCEL,
                       gtk.STOCK_OPEN,
                       gtk.RESPONSE_OK))

        chooser.set_preview_widget(self.preview)
        chooser.connect('selection-changed', self.on_chooser_update_preview)

        response = chooser.run()
        path = chooser.get_filename()
        chooser.destroy()
        gtk_do_pending()

        if response != gtk.RESPONSE_OK:
            return

        print path

        self.upload_file(path)

    # from http://taggen.googlecode.com/svn-history/r95/trunk/src/taggen.py.in
    def on_chooser_update_preview(self, filechooser, data=None):
        filename = filechooser.get_preview_filename()
        print 'preview for', filename

        if filename is None or os.path.isdir(filename):
            filechooser.set_preview_widget_active(False)
            self.avatar_pixbuf = None
            return

        pixbuf = gtk.gdk.pixbuf_new_from_file_at_size(filename, 128, 128)
        if pixbuf is not None:
            self.preview.set_from_pixbuf(pixbuf)
            filechooser.set_preview_widget_active(True)

    def update_button(self):
        if self.time == -1:
            gtk.main_quit()

        self.bt.set_label("Close (%s)" % self.time)
        self.time -= 1
        return True

if __name__ == "__main__":
    config_file = os.path.expanduser("~/.puff")
    assert os.path.exists(config_file), "You need a '%s'" % config_file

    config = ConfigParser.ConfigParser()
    config.read([config_file])

    FileShare(sys.argv[1:])
    gtk.main()
